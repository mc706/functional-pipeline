from operator import add, mul
from unittest import TestCase

from functional_pipeline import pipeline


class PipelineTestCase(TestCase):

    def test_function_pipeline_happy_path(self):
        result = pipeline(10, [
            (add, 1),
            (mul, 2),
        ])
        self.assertEqual(result, 22)

    def test_function_pipeline_native_functions(self):
        result = pipeline(range(0, 100), [(map, lambda x: x * 2), list])
        self.assertEqual(len(result), 100)
        self.assertTrue(isinstance(result, list))
