# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 0.6.1 - (2021-01-29)
---

### Changes
* Added python 3.9 to support matrix in pypi


## 0.6.0 - (2020-01-24)
---

### New
* Add `filter_where` function


## 0.5.0 - (2020-01-24)
---

### New
* Add `scanl` helper function 


## 0.4.0 - (2020-01-14)
---

### New
* Add `foldl` helper function 


## 0.3.2 - (2019-03-26)
---

### Fixes
* Fix broken documentation


## 0.3.1 - (2019-02-19)
---

### Changes
* Change helper functions to be curried functions instead of partially applied functions


## 0.3.0 - (2019-02-18)
---

### New
* Add `flatmap` shortcut function
* Add `zipmap` utiltity function


## 0.2.0 - (2019-02-16)
---

### New
* Add `not_none` method

### Changes
* Update tests to test all documentation examples

### Fixes
* Fix readthedocs deployment
* Reduce install footprint to speed up CI
* Fix gitlab pages deploy


## 0.1.1 - (2019-02-16)
---

### Changes
* Update documentation and docstrings


## 0.1.0 - (2019-02-15)
---

### New
* Initial Setup
* Add CI setup
* Add pipeline
* Add String Helper Class
* Add Basic Utility Functions


