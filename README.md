# Functional Pipeline
[![PyPI version](https://badge.fury.io/py/functional-pipeline.svg)](https://badge.fury.io/py/functional-pipeline)
[![pipeline status](https://gitlab.com/mc706/functional-pipeline/badges/master/pipeline.svg)](https://gitlab.com/mc706/functional-pipeline/commits/master)
[![coverage report](https://gitlab.com/mc706/functional-pipeline/badges/master/coverage.svg)](https://gitlab.com/mc706/functional-pipeline/commits/master)
[![PyPI](https://img.shields.io/pypi/pyversions/functional-pipeline.svg)](https://pypi.org/project/functional-pipeline/)
[![Documentation Status](https://readthedocs.org/projects/functional-pipeline/badge/?version=latest)](https://functional-pipeline.readthedocs.io/en/latest/?badge=latest)


Functional languages like Haskell, Elixir, and Elm have pipe functions that allow
the results of one function to be passed to the next function. 

Using functions from `functools`, we can build composition in python, however it is not
nearly as elegant as a well thought out pipeline. 

This library is designed to make the creation of a functional pipeline easier in python.

```python 
>>> from operator import add, mul

>>> from functional_pipeline import pipeline, tap

>>> result = pipeline(
...     10,
...     [
...         (add, 1),
...         (mul, 2)
...     ]
... )

>>> result
22

```

This pattern can be extended for easily dealing with lists or generators.

```python
>>> from functional_pipeline import pipeline, String, join

>>> names = [
...     "John",
...     "James",
...     "Bill",
...     "Tiffany",
...     "Jamie",
... ]

>>> result = pipeline(
...     names,
...     [
...         (filter, String.startswith('J')),
...         (map, lambda x: x + " Smith"),
...         join(", "),
...     ]
... )

>>> result
'John Smith, James Smith, Jamie Smith'

```
