Since a lot of the manipulation that happens inside of pipelines 
are happening to strings, this library provides a `String` helper class.

This helper class curries all of the functions that are available on a 
string instance in reverse order. 

For Example

```python
>>> from functional_pipeline import String

>>> String.startswith('H')('Hello World!') == 'Hello World!'.startswith('H')
True

>>> String.endswith('!')('Hello World!') == 'Hello World!'.endswith('!')
True

```

These curried functions allow for easy use inside of pipelines:

```python
>>> from functional_pipeline import pipeline, String

>>> words = ['this', 'that', 'other', 'foo', 'bar']

>>> starts_with_t = pipeline(
...     words,
...     [
...         (filter, String.startswith('t')),
...         list,
...     ]
... )

>>> starts_with_t
['this', 'that']

```

Available Methods on `String`

* capitalize
* casefold
* center
* count
* encode
* endswith
* expandtabs
* find
* format
* index
* isalnum
* isalpha
* isdecimal
* isidentifier
* islower
* isnumeric
* isprintable
* isspace
* istitle
* isupper
* join
* ljust
* lower
* lstrip
* maketrans
* partition
* replace
* rfind
* rindex
* rpartition
* rsplit
* rstrip
* split
* splitlines
* startswith
* swapcase
* title
* translate
* upper
* zfill
