# Welcome to Functional Pipeline's Documentation!

<table>
    <tr>
        <th>Documentation:</th>
        <td><a href="https://functional-pipeline.readthedocs.io/">https://functional-pipeline.readthedocs.io/</a></td>
    </tr>
    <tr>
        <th>Source Code:</th>
        <td><a href="https://gitlab.com/mc706/functional-pipeline">https://gitlab.com/mc706/functional-pipeline</a></td>
    </tr>
    <tr>
        <th>Issue Tracker:</th>
        <td><a href="https://gitlab.com/mc706/functional-pipeline/issues">https://gitlab.com/mc706/functional-pipeline/issues</a></td>
    </tr>
    <tr>
        <th>PyPI:</th>
        <td><a href="https://pypi.org/project/functional-pipeline/">https://pypi.org/project/functional-pipeline/</a></td>
    </tr>
</table>


Functional languages like Haskell, Elixir, and Elm have pipe functions that allow
the results of one function to be passed to the next function. 

Using functions from `functools`, we can build composition in python, however it is not
nearly as elegant as a well thought out pipeline. 

This library is designed to make the creation of a functional pipeline easier in python.

```python 
from operators import add, multiply
from functional_pipeline import pipeline, tap

result = pipeline(
    10,
    [
        (add, 1),
        (multiply, 2)
    ]
)
print(result)  # 22
```

This pattern can be extended for easily dealing with lists or generators.

```python
from functional_pipeline import pipeline, String, join

names = [
    "John",
    "James",
    "Bill",
    "Tiffany",
    "Jamie",
]

result = pipeline(
    names,
    [
        (filter, String.startswith('J')),
        (map, lamdba x: x + " Smith")
        join(", ")
    ]
)
print(result)  # "John Smith, James Smith, Jamie Smith"
```


## Installation
This project is distributed via `pip`. To get started:

```
pip install functional-pipeline
```
